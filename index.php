<?php include("src/dist/includes/header.php"); ?>

<!-- <style>
body, html{
  overflow-x: hidden;
}
</style> -->
  
    <video autoplay muted loop playsinline class="fix-video">
      <source src="src/dist/img/Bg_indexMp4.mp4" type="video/mp4">
    </video>
    <div class="video-mask">
    </div>
    <header id="section_header">
      <div class="slider-wrap">

        <div class="slider main-slider">
          <?php
            $header_img = [
              "src/dist/img/home/Bg_index01.png",
              "src/dist/img/home/Bg_index02.png",
              "src/dist/img/home/Bg_index03.png",
              "src/dist/img/home/Bg_index04.png",
              "src/dist/img/home/Bg_index05.png"
            ];
            $i=0;
            while($i<5): 
           ?>
          <div>
            <div class="img-wrap">
              <img src="<?php echo $header_img[$i]; ?>" alt="">
            </div>
          </div>
          <?php
            $i++;
            endwhile; 
          ; ?>

        </div>
        <div class="main-logo-area">
          <img src="src/dist/img/LOGO_Afbeatz.png" alt="">
        </div>
      </div>
    </header>


    <!-- section features -->
    <section id="features" data-aos="fade-up">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title">
              <div class="decor-line"></div>
              <div class="img-wrap">
                <img src="src/dist/img/title-style-02.png" alt="">
              </div>
              <div class="decor-line"></div>
            </div>
          </div>
        </div>
        <div class="row">
          
          <?php 
            $masks = [
              "src/dist/img/BG_NEWS_L.png",
              "src/dist/img/BG_NEWS_C.png",
              "src/dist/img/BG_NEWS_R.png"
            
            ]
           ?>


          <?php
            $i=0;
            while($i<2): 
           ?>
          <div class="col-12 col-lg-4 card-col">
            <a href="">
              <div class="feature-card-wrap">
                <div class="card-title">
                  <h3>這是標題</h3>
                </div>

                <div class="card-img">
                  <div class="img-wrap">
                    <img src="https://placekitten.com/g/600/400" alt="">
                  </div>
                  <div class="mask-img">
                    <img src="<?php echo $masks[$i]?>" alt="">
                  </div>
                  <div class="mask-img mobile">
                    <img src="src/dist/img/BG_NEWS_C.png" alt="">
                  </div>
                </div>

                <div class="card-text">
                  <div class="text-wrap">
                    <p>
                      我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。
                      過鐵道時，他先將橘子散放在地上，自己慢 慢爬下，再抱起橘子走。到這邊時，我趕緊去攙他。
                      
                    
                    </p>
                  </div>
                </div>
              </div>
            </a>
          </div>


          <?php
            $i++;
            endwhile; 
          ; ?>
          <div class="col-12 col-lg-4 card-col">
            <a href="">
              <div class="feature-card-wrap">
                <div class="card-title">
                  <h3>這是標題這是標題這是標題s</h3>
                </div>

                <div class="card-img">
                  <div class="img-wrap">
                    <img src="https://placekitten.com/g/600/400" alt="">
                  </div>
                  <div class="mask-img">
                    <img src="<?php echo $masks[$i]?>" alt="">
                  </div>
                  <div class="mask-img mobile">
                    <img src="src/dist/img/BG_NEWS_C.png" alt="">
                  </div>
                </div>

                <div class="card-text">
                  <div class="text-wrap">
                    <p>
                      我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。
                      過鐵道時，他先將橘子散放在地上，自己慢 慢爬下，再抱起橘子走。到這邊時，我趕緊去攙他。
                      
                    
                    </p>
                  </div>
                </div>
              </div>
            </a>
          </div>

        </div>
      </div>
    </section>


    <section id="group-description" data-aos="fade-up">
      <!-- <img class="bg-img" src="src/dist/img/BG_ABOUT.png" alt=""> -->
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title">
              <img src="src/dist/img/LOGO_Afbeatz.png" alt="">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="text-wrap">
              <p class="p-1">
                Afbeatz 是 "After + Beatz" 的合體字
                <br>
                +
                <br>
                複數形式的S，音變為Z
              </p>
              <p class="p-2">
                內文內文內文內文內文內文內文內文內文內文<br>
                內文內文內文內文內文內文內文<br>
                內文內文內文內文內文內文內文內文內<br>
                內文內文內文內<br>
              </p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="about-btn">
              <a href="">
                <button>ABOUT</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="schedule" data-aos="fade-up">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title">
              <div class="decor-line"></div>
              <div class="img-wrap">
                <img class="sche" src="src/dist/img/title-schedule.png" alt="">
              </div>
              <div class="decor-line"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            
            <div class="list-wrap">
              <ul class="lists" id="accordion">

                <?php
                  $i=0;
                  while($i<8): 
                ?>

                <li>
                  <div id="heading<?php echo $i; ?>">
                    
                    <div class="list" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                      <div class="date col-12 col-lg-2">2018.12.23</div>
                      <div class="text col-12 col-lg-8">我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。</div>
                      <div class="cate col-12 col-lg-2"><span>compaign</span></div>
                    </div>
                    
                  </div>

                  <div id="collapse<?php echo $i; ?>" class="collapse collapsed" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordion">
                    <div class="card-body">
                    我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。
                    我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。
                    我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。
                    我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。
                    我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。
                    </div>
                  </div>
                </li>

    
                <!-- <li class="list">
                  <a href="">
                    <div class="date col-12 col-lg-2">2018.12.23</div>
                    <div class="text col-12 col-lg-8">我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。</div>
                    <div class="cate col-12 col-lg-2"><span>compaign</span></div>
                  </a>
                  
                </li> -->

                <?php
                  $i++;
                  endwhile; 
                ; ?>



              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="see-more-btn">
              <a href="">
                <button>SEE MORE</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="youtube-slider" data-aos="fade-up">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="slider-wrap">
              <div class="slider video-slider">

                <?php
                  $i=0;
                  while($i<5): 
                 ?>

                <div class="video-wrap">
                  <iframe src="https://www.youtube.com/embed/djsSqan0jGs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>

                <?php
                  $i++;
                  endwhile; 
                ; ?>



              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="members" data-aos="fade-up">
      <div class="container">
        
          
        <div class="row">

          <?php
            $i=0;
            while($i<50): 
          ?>
          
          <div class="col-md-3 col-lg-2 col-4">
            <a class="box" data-fancybox="mygallery" title="<h5 style='text-align:center;'>name</h5>" href="https://placekitten.com/g/600/600">
              <div class="member">
                <!-- <img src="https://placekitten.com/g/600/600" alt=""> -->
              </div>
            </a>
            
          </div>


          <?php
            $i++;
            endwhile; 
          ; ?>

        </div>
         
        
      </div>
    </section>
<?php include("src/dist/includes/footer.php"); ?>
<script>
obj_hover_rotate("body", ".main-logo-area", ".main-logo-area img");
</script>
</body>
</html>
      
    