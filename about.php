<?php include("src/dist/includes/header.php"); ?>
<style>
    body {
        background-color: #59128c;
    }
    .slick-slider {
    /* touch-action: auto;
    -ms-touch-action: auto; */
    }
</style>
<div class="bg-img">
</div>
<section id="about">
    
    <div class="container page-title">
        <div class="row">
            <div class="col-12">
                <div class="title">
                    <!-- <img src="src/dist/img/about/Title_about.png" alt=""> -->
                </div>
            </div>
        </div>
    </div>
    <div class="container page-content" data-aos="fade-up">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <p class="p-1">
                        Afbeatz 是 "After + Beatz" 的合體字
                        <br>
                        +
                        <br>
                        複數形式的S，音變為Z
                    </p>
                    <p class="p-2">
                        內文內文內文內文內文內文內文內文內文內文<br>
                        內文內文內文內文內文內文內文<br>
                        內文內文內文內文內文內文內文內文內<br>
                        內文內文內文內<br>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php  
        $img_urls = [
            'src/dist/img/about/Pic_Jin_s.png',
            'src/dist/img/about/Pic_Youei_s.png',
            'src/dist/img/about/Pic_Takae_s.png',
            'src/dist/img/about/Pic_Dino_s.png',
            'src/dist/img/about/Pic_Chumi_s.png',
            'src/dist/img/about/Pic_Mega_s.png',
            'src/dist/img/about/Pic_Jsun_s.png'
        ];
    ?>
  
    <div class="members-pc">

        <?php foreach($img_urls as $key=>$url): ?>

        <div class="member-wrap">
            <img data-aos="fade-up" slide="<?php echo $key;?>" class="pic slick-img" src="<?php echo $url;?>" alt="">
            <div class="profile" data-aos="fade-up">
                <h2>XXXXXXXXXXXXX<br>
                    XXXXXXXXXXX
                </h2>
                <div class="content">
                    <p>
                        內文內文內文內文內文內文內文內文內文內文<br>
                        內文內文內文內文內文內文內文<br>
                        內文內文內文內文內文內文內文內文內<br>
                    </p>
                </div>
            </div>
        </div>

        <?php endforeach; ?>
    </div>

    <div class="members-mb container">
        

        <?php foreach($img_urls as $key=>$url): ?>
        <div class="row mb">
            <div class="col-12 col-md-4 <?php echo $key%2?'order-md-8':''; ?>">
                <div data-aos="fade-up" class="pic-wrap">
                    <img class="slick-img" slide="<?php echo $key;?>" src="<?php echo $url;?>" alt="">
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="content-wrap">
                    <div data-aos="fade-up" class="profile <?php echo $key%2?'tr-r':'tr-l'; ?>">
                        <h2>內文內文內文內文內文內文內文內文內文內文
                        </h2>
                        <div class="content">
                            <p>
                                內文內文內文內文內文內文內文內文內文內文<br>
                                內文內文內文內文內文內文內文<br>
                                內文內文內文內文內文內文內文內文內<br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>






    <div class="light-box">
        <button class="close">✕</button>
        <div class="container light-box-container">

            <?php foreach($img_urls as $key=>$url): ?>

            <div>
                <div class="row row100vh">
                    <div class="col-12 col-md-4">
                        <div class="pic-wrap">
                            <img class="pic pic<?php echo $key+1;?>" src="<?php echo $url;?>" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="content-wrap">
                            <div class="profile">
                                <h2>內文內文內文內文內文內文內文內文內文內文
                                </h2>
                                <div class="content"> 
                                    <p>
                                        內文內文內文內文內文內文內文內文內文內文<br>
                                        內文內文內文內文內文內文內文<br>
                                        內文內文內文內文內文內文內文內文內<br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endforeach; ?>


        </div>
    </div>
</section>

<?php include("src/dist/includes/footer.php"); ?>
</body>
</html>